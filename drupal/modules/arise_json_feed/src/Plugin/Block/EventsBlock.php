<?php
/**
 * @file
 * Contains \Drupal\arise_json_feed\Plugin\Block\EventsBlock.
 */
namespace Drupal\arise_json_feed\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
/**
 * Provides a 'Example: configurable text string' block.
 *
 * Drupal\Core\Block\BlockBase gives us a very useful set of basic functionality
 * for this configurable block. We can just fill in a few of the blanks with
 * defaultConfiguration(), blockForm(), blockSubmit(), and build().
 *
 * @Block(
 *   id = "example_configurable_text",
 *   admin_label = @Translation("Title of first block (example_configurable_text)")
 * )
 */
class EventsBlock extends BlockBase {
  // ini_set('allow_url_fopen', 1);

	/**
   * {@inheritdoc}
   */
  public function build() {

    $config = $this->getConfiguration();
    $arise_api_key =  $config['arise_api_key'];
    $feedtag =$config['feedtag'];
    $content_type = $config['content_type'];
    $content_link = $config['link_url'];
    $content_text = $config['link_text'];
    $max_feed = $config['max_feed'];

    $url = 'http://www.preventionweb.net/v2/api/v1/tagged_content/get/output.json?tag='.$feedtag.'&content_type='.$content_type.'&number='.$max_feed.'&api_key='.$arise_api_key;

    /* CURL */
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_TIMEOUT, 5);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $data = curl_exec($ch);
    curl_close($ch);

    /*
    for testing
   
    $data = '
    {
      "results":[
        {
          "id":"44470",
          "title":"World Humanitarian Summit 2016",
          "url":"http:\/\/www.preventionweb.net\/go\/44470",
          "sources":"United Nations Office for the Coordination of Humanitarian Affairs - Headquarters",
          "locations":[
            {
              "date_start":"2016-05-26",
              "date_end":"2016-05-27",
              "country":"Turkey",
              "city":"Istanbul",
              "venue":""
            }
          ]
        },
        {
          "id":"44475",
          "title":"World Humanitarian Summit 2016",
          "url":"http:\/\/www.preventionweb.net\/go\/44470",
          "sources":"United Nations Office for the Coordination of Humanitarian Affairs - Headquarters",
          "locations":[
            {
              "date_start":"2016-05-26",
              "date_end":"2016-05-26",
              "country":"Turkey",
              "city":"Istanbul",
              "venue":""
            }
          ]
        },
        {
          "id":"44475",
          "title":"World Humanitarian Summit 2016",
          "url":"http:\/\/www.preventionweb.net\/go\/44470",
          "sources":"United Nations Office for the Coordination of Humanitarian Affairs - Headquarters",
          "locations":[
            {
              "date_start":"2015-12-28",
              "date_end":"2016-01-04",
              "country":"Turkey",
              "city":"Istanbul",
              "venue":""
            }
          ]
        },
        {"id":"30512","title":"21st Conference of the Parties to the UNFCCC and 11th meeting of the Parties to the Kyoto Protocol (COP 21\/CMP 11)","url":"http:\/\/www.preventionweb.net\/go\/30512","sources":"United Nations Framework Convention on Climate Change","locations":[{"date_start":"2015-11-30","date_end":"2015-12-11","country":"France","city":"Paris","venue":""}]}],
      "total":2,
      "status":200
    }';
    
    {
      "results":[
        {
          "id":"45940",
          "title":"Fresh call for resilient private sector",
          "url":"http:\/\/www.preventionweb.net\/go\/45940",
          "sources":"United Nations Office for Disaster Risk Reduction - New York UNHQ Liaison Office",
          "published":"28\/09\/2015",
          "date":"2015-09-27"
        },
        {
          "id":"45890","title":"US: AECOM wins 2015 ND-GAIN corporate adaptation prize for UN disaster resilient scorecard","url":"http:\/\/www.preventionweb.net\/go\/45890","sources":"Business Wire","published":"25\/09\/2015","date":"2015-09-23"},
        {"id":"45576","title":"Indonesia: Business key for Sendai Framework success","url":"http:\/\/www.preventionweb.net\/go\/45576","sources":"United Nations Office for Disaster Risk Reduction - Regional Office for Asia and Pacific","published":"31\/08\/2015","date":"2015-08-28"},
        {"id":"45529","title":"US: Walmart and the Walmart Foundation mark 10th anniversary of Hurricane Katrina with $25 million commitment to support disaster recovery and resiliency efforts worldwide","url":"http:\/\/www.preventionweb.net\/go\/45529","sources":"Wal-Mart Stores Inc.","published":"26\/08\/2015","date":"2015-08-21"},
        {"id":"45054","title":"Viet Nam: Tourism sector embraces Sendai Framework","url":"http:\/\/www.preventionweb.net\/go\/45054","sources":"United Nations Office for Disaster Risk Reduction - Regional Office for Asia and Pacific","published":"06\/07\/2015","date":"2015-07-06"
        }
      ],
      "total":5,
      "status":200
    }
    
    */

    $json = json_decode($data, true);
    
    // var_dump($data);
    
    $content = '';
    
    $status_class = 'status-'.$json['status'];
    
    if ($json['status'] == 200) {
      if ($json['total'] > 0) {
        $results = $json['results'];

        /* events */
        if ($content_type == 5) {

          foreach($results as $index => $event) {
            if ($index == $max_feed) break;

            $content .= '<div id="event-id-'.$event['id'].'" class="col-md-3 event">';
            $content .= '<div class="event-location">';

            $locations = $event['locations'];
            $date_start = $locations[0]['date_start'];
            $date_end = $locations[0]['date_end'];

            $time_start = strtotime($date_start);
            $time_end = strtotime($date_end);
            
            $date_start_year = date('Y', $time_start);
            $date_start_month = date('M', $time_start);
            $date_start_day = date('d', $time_start);

            $date_end_year = date('Y', $time_end);
            $date_end_month = date('M', $time_end);
            $date_end_day = date('d', $time_end);

            $datecontent = '';
            /* one day event */
            if ($time_start == $time_end) {
              $datecontent .= '<span class="event-date-elem">'.$date_start_day.'&nbsp;'.$date_start_month.'&nbsp;'.$date_start_year.'</span>';
            }
            /* on the same year */
            elseif ($date_start_year == $date_end_year) {
              /* on the same month, same year */
              if ($date_start_month == $date_end_month) {
                $datecontent .= '<span class="event-date-elem event-date-day first">'.$date_start_day.'</span>';
                $datecontent .= '<span class="event-date-elem event-date-dash">-</span>';
                $datecontent .= '<span class="event-date-elem event-date-day">'.$date_end_day.'</span>';
                $datecontent .= '<span class="event-date-elem event-date-month">'.$date_start_month.'</span>';
                $datecontent .= '<span class="event-date-elem event-date-year">'.$date_start_year.'</span>';
              }
              /* on 2 months, same year */
              else {
                $datecontent .= '<span class="event-date-elem event-date-day first">'.$date_start_day.'</span>';
                $datecontent .= '<span class="event-date-elem event-date-month">'.$date_start_month.'</span>';
                $datecontent .= '<span class="event-date-elem event-date-dash">-</span>';
                $datecontent .= '<span class="event-date-elem event-date-day">'.$date_end_day.'</span>';
                $datecontent .= '<span class="event-date-elem event-date-month">'.$date_end_month.'</span>';
                $datecontent .= '<span class="event-date-elem event-date-year last">'.$date_start_year.'</span>';
              }
            }
            /* on 2 years */
            else {
              $datecontent .= '<span class="event-date-elem event-date-day first">'.$date_start_day.'</span>';
              $datecontent .= '<span class="event-date-elem event-date-month">'.$date_start_month.'</span>';
              $datecontent .= '<span class="event-date-elem event-date-year">'.$date_start_year.'</span>';
              $datecontent .= '<span class="event-date-elem event-date-dash">-</span>';
              $datecontent .= '<span class="event-date-elem event-date-day">'.$date_end_day.'</span>';
              $datecontent .= '<span class="event-date-elem event-date-month">'.$date_end_month.'</span>';
              $datecontent .= '<span class="event-date-elem event-date-year last">'.$date_end_year.'</span>';
            }

            $content .= '<span class="event-location-country">'.$locations[0]['country'].'</span>';
            $content .= '<span class="event-location-city">('.$locations[0]['city'].')</span>';
            $content .= '</div>';
            $content .= '<div class="event-date">'.$datecontent.'</div>';
            $content .= '<div class="event-summary"><a href="'.$event['url'].'" class="event-link">'.$event['title'].'</a></div>';
            $content .= '</div>';
          }
        }

        /* news */
        elseif ($content_type == 2) {

          foreach ($results as $index => $news) {
            if ($index == $max_feed) break;

            // $news_time = strtotime("2011-01-07");//strtotime($news['date']);

            // $datecontent = '<span class="news-date-elem news-date-day first">'date('d', $news_time).'</span>';
            // $datecontent .= '<span class="news-date-elem news-date-month">'.date('M', $news_time).'</span>';
            // $datecontent .= '<span class="news-date-elem news-date-year last">'.date('Y', $news_time).'</span>';

            $content .= '<div id="news-id-'.$news['id'].'" class="col-md-3 news">';
            $content .= '<div class="news-date">'.$news['date'].'</div>';
            $content .= '<div class="news-title"><a href="'.$news['url'].'" class="news-link">'.$news['title'].'</a></div>';
            $content .= '<div class="news-sources">'.$news['sources'].'</div>';
            $content .= '</div>';
          }

        }

        $content .= '<div class="tab-footer"><span><a class="more-link" href="'.$content_link .'" target="_blank">'.$content_text .'</a></span></div>';
       // $content .= '</div></div></div>';
      }
      else {
        $content = 'No available content.';
      }
    }
    else {
      $content = 'Remote server error.';
      // $content .= $url;
    }

    return array(
      '#type' => 'markup',
      '#markup' => $content,
    );

  }
    /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);    
    $config = $this->getConfiguration();
    $content_type_array = range(1, 99);
    $max_feed_array = range(1, 4);
    $form['arise_api_key'] = array(
      '#type' => 'textfield',
      '#title' => t('ARISE API key'),
      '#default_value' => $config['arise_api_key'],
      '#maxlength' => 128,
      '#description' => t('ARISE API key for getting feed.'),
      '#required' => TRUE,
    );
    $form['feedtag'] = array(
      '#type' => 'textfield',
      '#title' => t('Tag'),
      '#default_value' => $config['feedtag'],
      '#maxlength' => 128,
      '#description' => t('Feed tag.'),
      '#required' => TRUE,
    );
    $form['link_url'] = array(
      '#type' => 'textfield',
      '#title' => t('Link'),
      '#default_value' => $config['link_url'],
      '#maxlength' => 128,
      '#description' => t('Feed tag.'),
      '#required' => TRUE,
    );
    $form['link_text'] = array(
      '#type' => 'textfield',
      '#title' => t('Text'),
      '#default_value' => $config['link_text'],
      '#maxlength' => 128,
      '#description' => t('Feed tag.'),
      '#required' => TRUE,
    );
    $form['content_type'] = array(
      '#type' => 'select',
      '#title' => t('Content type'),
      '#options' => array_combine($content_type_array, $content_type_array),
      '#default_value' => $config['content_type'],
      '#description' => t('Content type ID.'),
      '#required' => TRUE,
    );
      $form['max_feed'] = array(
      '#type' => 'select',
      '#title' => t('Maximum number of items to display'),
      '#options' => array_combine($max_feed_array, $max_feed_array),
      '#default_value' => $config['max_feed'],
      '#description' => $this->t('This will set the maximum allowable number of feed elements in the feed block config form'),
      '#required' => TRUE,
    );
    return $form;
  }
    /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->setConfigurationValue('arise_api_key', $form_state->getValue('arise_api_key'));
    $this->setConfigurationValue('feedtag', $form_state->getValue('feedtag'));
    $this->setConfigurationValue('content_type', $form_state->getValue('content_type'));
    $this->setConfigurationValue('max_feed', $form_state->getValue('max_feed'));
    $this->setConfigurationValue('link_url', $form_state->getValue('link_url'));
    $this->setConfigurationValue('link_text', $form_state->getValue('link_text'));
  } 
  
}
