/*
 * jQuery wraplines plugin
 *
 * Copyright (c) 2010 Paul Bennett (http://pmbennett.net)
 * Licensed under the MIT License:
 *   http://www.opensource.org/licenses/mit-license.php
 * modified by rad.
 *
 */

jQuery.fn.wraplines = function(options) {
  var options = jQuery.extend({
    lineClass: 'line', /* prefix for class name to be added to line wrapper element, tune css accordingly */
  }, options);

  return this.each(function() {
    if (options.lineClass.length < 1) {
      options.lineClass = 'line';
    }

    var element = jQuery(this);

    jQuery(element).html(function(ind, htm) {
      return '<span>' + jQuery(element).text().trim().replace(/\s/g, '</span> <span>') + '</span>';
    });

    var nlines = 0;
    var offsetTop = 0;
    var wordClassPrefix = 'temp_w_line_';

    /* loop every word */
    jQuery('span', element).each(function() {
      var spanOffset = jQuery(this).offset();
      /* detect end of line */
      if (spanOffset.top > offsetTop) {
        offsetTop = spanOffset.top;
        nlines++;
      }
      jQuery(this).addClass(wordClassPrefix + nlines);
    });

    /* loop wrapped lines */
    for (var line = 1; line <= nlines; line++) {
      var classes = '';
      var lineClassPrefix = 'temp_wrap_line_' + line;

      classes += ' ' + options.lineClass;
      classes += ' ' + lineClassPrefix;

      if (line == 1) {
        classes += ' first';
      }
      if (line == nlines) {
        classes += ' last';
      }

      jQuery('.' + wordClassPrefix + line, element).wrapAll('<span class="' + classes + '" />').append(' ');

      /* line text */
      var innerText = jQuery('.' + lineClassPrefix, element).text().trim();

      jQuery('.' + lineClassPrefix, element).removeClass(lineClassPrefix).html(function() {
        return innerText;
      });
    }
  });
};
