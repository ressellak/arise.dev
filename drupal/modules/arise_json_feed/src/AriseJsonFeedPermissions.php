<?php

/**
 * @file
 * Contains \Drupal\devel_generate\DevelGeneratePermissions.
 */

namespace Drupal\arise_json_feed;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;

/**
 * Provides dynamic permissions of the filter module.
 */
class AriseJsonFeedPermissions implements ContainerInjectionInterface {

  /*
   * A permissions callback.
   *
   * @see arise_json_feed.permissions.yml.
   *
   * @return array
   */
  function permissions() {
    $permissions = array(
     'administer arise_json_feed' => array(
       'title' => t('Administer ARISE JSON Feed'),
     ),
   );
    return $permissions;
  }

}
