<?php

/**
 * @file
 * Contains \Drupal\myblock\Controller\MyBlockController.
 */

namespace Drupal\myblock\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;

/**
 * Controller routines for block example routes.
 */
class MyBlockController extends ControllerBase {

  /**
   * Controller method to explain what the module is about.
   */
  public function description() {
    // Make a link from a route to the block admin page.
    $url = Url::fromRoute('block.admin_display');
    $block_admin_link = $this->l($this->t('the block admin page'), $url);

    // $url = Url::fromRoute('myblock.eventsdescription');
    // $text = 'a text';
    // $links[] = \Drupal::l(t($text), $url);

    // $form = \Drupal::formBuilder()->getForm('Drupal\myblock\Form\Settings\MyBlockSettingsForm');

    $content = '<p>'.$this->t('This modules provides blocks for ARISE media content: Events, News, Library, Partners and Members. Enable and configure them on !block_admin_link.', ['!block_admin_link' => $block_admin_link]).'</p>';
    $content .= '<p>'.$this->t('Blocks provided are: ').'</p>';
    $content .= '<ul><li>'.$this->t('Events block: provides last ARISE events').'</li>';
    $content .= '<li>'.$this->t('News block: last ARISE News').'</li>';
    $content .= '<li>'.$this->t('Members block: enables a block of ARISE members list with a sort/filter form').'</li>';
    $content .= '<li>'.$this->t('Library block: provides last PreventionWeb publications tagged with "ARISE"').'</li>';
    $content .= '<li>'.$this->t('Partners block: shows a small summary about ARISE partners').'</li></ul>';

    return array(
      '#markup' => $content,
    );
  }

}
