<?php

/**
 * @file
 * Contains \Drupal\arise_json_feed\Controller\AriseJsonFeedController.
 */

namespace Drupal\arise_json_feed\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;

/**
 * Controller routines for block example routes.
 */
class AriseJsonFeedController extends ControllerBase {

  /**
   * A simple controller method to explain what the block is about.
   */
  public function description() {
    // Make a link from a route to the block admin page.
    $url = Url::fromRoute('block.admin_display');
    $block_admin_link = $this->l($this->t('the block admin page'), $url);

    // Put the link into the content.
    $build = array(
      '#markup' => $this->t('The Block provides blah blah, enable and configure them on !block_admin_link.', ['!block_admin_link' => $block_admin_link]),
    );

    return $build;
  }

}
