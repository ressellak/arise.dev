<?php
/**
 * @file
 * Contains \Drupal\myblock\Plugin\Block\NewsBlock.
 */
namespace Drupal\myblock\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides ARISE last news.
 *
 * @Block(
 *   id = "arise_news_block",
 *   admin_label = @Translation("ARISE news block"),
 *   category = @Translation("ARISE Media")
 * )
 */
class NewsBlock extends BlockBase {

  const MAX_LIMIT = 50;
  const URL_PREFIX = 'http://www.preventionweb.net/v2/api/v1/content_by_theme/get/output.json';

  /**
   * {@inheritdoc}
   */
  public function build() {

    /* get saved configuration */
    $config = $this->getConfiguration();

    $arise_api_key =  $config['arise_api_key'];
    $content_type = $config['content_type'];
    $themeid = $config['themeid'];
    $subtype = $config['subtype'];
    $num_items = $config['num_items'];
    $more_link = $config['link_url'];
    $more_text = $config['link_text'];

    $url = self::URL_PREFIX.'?ct='.$content_type.'&theme_ids='.$themeid.'&type='.$subtype.'&api_key='.$arise_api_key.'&limit='.$num_items;

    /* CURL */
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_TIMEOUT, 5);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $data = curl_exec($ch);

    if (curl_errno($ch)) { 
      \Drupal::logger('myblock')->error('CURL error: '.curl_error($ch). 'URL: '.$url);
    }
    else { 
      curl_close($ch); 
    }

    $json = json_decode($data, true);
    \Drupal::logger('myblock')->notice($data);

    if ($num_items > $json['total']) {
      \Drupal::logger('myblock')->notice('Limit exceeds length of results');
    }
    if ($num_items == 0) {
      \Drupal::logger('myblock')->notice('Number of items to be displayed was set to 0. Block will show all items but max '.self::MAX_LIMIT);
    }

    /* init content */
    $content = '';

    return array(
      '#theme' => 'arise_news_feed_block',
      '#content' => $content,
      '#status' => $json['status'],
      '#total' => $json['total'],
      '#num_items' => $num_items,
      '#results' => $json['results'],
      '#more_link' => $more_link,
      '#more_text' => $more_text,
      '#attached' => array(
        'library' =>  array(
          'myblock/base'
        ),
      ),
      '#attributes' => [],
    );

  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $groupid_options = range(1, 99);
    $num_items_options = range(0, self::MAX_LIMIT);

    /* define configuration form items */
    $form['arise_api_key'] = array(
      '#type' => 'textfield',
      '#title' => t('ARISE API key'),
      '#default_value' => $config['arise_api_key'],
      '#maxlength' => 128,
      '#description' => t('ARISE API key for getting feed.'),
      '#required' => TRUE,
    );
    $form['content_type'] = array(
      '#type' => 'select',
      '#title' => t('Content type'),
      '#options' => array_combine($groupid_options, $groupid_options),
      '#default_value' => $config['content_type'],
      '#description' => t('Content type ID'),
      '#required' => TRUE,
    );
    $form['themeid'] = array(
      '#type' => 'select',
      '#title' => t('Theme ID'),
      '#options' => array_combine($groupid_options, $groupid_options),
      '#default_value' => $config['themeid'],
      '#description' => t('Theme ID'),
      '#required' => TRUE,
    );
    $form['subtype'] = array(
      '#type' => 'select',
      '#title' => t('Sub-type ID'),
      '#options' => array_combine($groupid_options, $groupid_options),
      '#default_value' => $config['subtype'],
      '#description' => t('Sub-type ID'),
      '#required' => TRUE,
    );
    $form['num_items'] = array(
      '#type' => 'select',
      '#title' => t('Number of items to display'),
      '#options' => array_combine($num_items_options, $num_items_options),
      '#default_value' => $config['num_items'],
      '#description' => $this->t('This will set the maximum allowable number of feed elements in the feed block config form'),
      '#required' => TRUE,
    );
    $form['link_url'] = array(
      '#type' => 'textfield',
      '#title' => t('Link'),
      '#default_value' => $config['link_url'],
      '#maxlength' => 256,
      '#description' => t('Feed tag.'),
      '#required' => TRUE,
    );
    $form['link_text'] = array(
      '#type' => 'textfield',
      '#title' => t('Text'),
      '#default_value' => $config['link_text'],
      '#maxlength' => 256,
      '#description' => t('Feed tag.'),
      '#required' => TRUE,
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    /* sumbit configuration */
    $this->setConfigurationValue('arise_api_key', $form_state->getValue('arise_api_key'));
    $this->setConfigurationValue('content_type', $form_state->getValue('content_type'));
    $this->setConfigurationValue('themeid', $form_state->getValue('themeid'));
    $this->setConfigurationValue('subtype', $form_state->getValue('subtype'));
    $this->setConfigurationValue('num_items', $form_state->getValue('num_items'));
    $this->setConfigurationValue('link_url', $form_state->getValue('link_url'));
    $this->setConfigurationValue('link_text', $form_state->getValue('link_text'));
  }

}
