<?php

/**
 * @file
 * Contains \Drupal\banner_block\Controller\BannerBlockController.
 */

namespace Drupal\banner_block\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;

/**
 * Controller routines for block example routes.
 */
class BannerBlockController extends ControllerBase {

  /**
   * Controller method to explain what the module is about.
   */
  public function description() {
    // Make a link from a route to the block admin page.
    $url = Url::fromRoute('block.admin_display');
    $block_admin_link = $this->l($this->t('the block admin page'), $url);
    
    $content = '';

    // $content = '<p>'.$this->t('This module enable blocks for banners with image and quote text. Enable and configure them on !block_admin_link' => $block_admin_link]).'</p>';

    return array(
      '#markup' => $content,
    );
  }

}
