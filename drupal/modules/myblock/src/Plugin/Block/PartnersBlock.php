<?php
/**
 * @file
 * Contains \Drupal\myblock\Plugin\Block\PartnersBlock.
 */
namespace Drupal\myblock\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides ARISE partners summary text.
 *
 * @Block(
 *   id = "arise_partners_block",
 *   admin_label = @Translation("ARISE partners block"),
 *   category = @Translation("ARISE Media")
 * )
 */
class PartnersBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    $config = $this->getConfiguration();

    $partners_text =  $config['partners_text'];
    $more_link = $config['link_url'];
    $more_text = $config['link_text'];
    
    $content = '<div class="container-fluid"><div class="row"><div class="col-md12"><p>'.$partners_text.'</p></div></div><div class="row"><div class="tab-footer"><span><a target="_blank" href="'.$more_link.'" class="more-link">'.$more_text.'</a></span></div></div></div>';

    return array(
      '#theme' => 'arise_partners_feed_block',
      '#content' => $content,
      '#text' => $partners_text,
      '#more_link' => $more_link,
      '#more_text' => $more_text,
      '#attached' => array(
        'library' =>  array(
          'myblock/base'
        ),
      ),
      '#attributes' => [],
    );

  }
    /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $groupid_options = range(1, 99);
    $num_items_options = range(0, 4);

    /* define configuration form items */
    $form['partners_text'] = array(
      // '#type' => 'text_format',
      '#type' => 'textarea',
      '#title' => t('Partners text'),
      '#default_value' => $config['partners_text'],
      '#description' => t('Text to put in partners tab.'),
      '#required' => TRUE,
    );
    $form['link_url'] = array(
      '#type' => 'textfield',
      '#title' => t('Link'),
      '#default_value' => $config['link_url'],
      '#maxlength' => 256,
      '#description' => t('Feed tag.'),
      '#required' => TRUE,
    );
    $form['link_text'] = array(
      '#type' => 'textfield',
      '#title' => t('Text'),
      '#default_value' => $config['link_text'],
      '#maxlength' => 256,
      '#description' => t('Feed tag.'),
      '#required' => TRUE,
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    /* sumbit configuration */
    $this->setConfigurationValue('partners_text', $form_state->getValue('partners_text'));
    $this->setConfigurationValue('link_url', $form_state->getValue('link_url'));
    $this->setConfigurationValue('link_text', $form_state->getValue('link_text'));
  } 

}
