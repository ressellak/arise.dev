(function($) {
  $(document).ready(function() {
    if ($('.sort-filter-controls').length) {
      $('.feed-members').mixItUp();
      $('.feed-members').on('mixEnd', function(e, state) {
        /* css .main-container overflow adapt */
        $('.main-container').addClass('overflow-hidden');

        /* even odd row update */
        $('.feed-members .mix').each(function (index, value) {
          $(this).removeClass('unhidden even odd');
          if ($(this).css('display') != 'none') {
            $(this).addClass('unhidden');
          }
        });
        $('.feed-members .mix.unhidden:even').addClass('even');
        $('.feed-members .mix.unhidden:odd').addClass('odd');
        
        //$('html, body').animate({scrollTop: $(this).parent().offset().top}, {duration: 200});
      });

      /* css .main-container overflow adapt */
      $('.country-dropdown-toggle').click(function() {
        $('.main-container').toggleClass('overflow-hidden');
      });
      $('.country-dropwown-menu').hover(
        function() {//hover in
          if ($('.main-container').hasClass('overflow-hidden')) {
            $('.main-container').removeClass('overflow-hidden');
          }
        },
        function() {//hover out
          if (!$('.main-container').hasClass('overflow-hidden')) {
            $('.main-container').addClass('overflow-hidden');
          }
        }
      );
    }
  });
})(jQuery);
