<?php
/**
 * @file
 * Contains \Drupal\myblock\Plugin\Block\LibraryBlock.
 */
namespace Drupal\myblock\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides ARISE publications list.
 *
 * @Block(
 *   id = "arise_library_block",
 *   admin_label = @Translation("ARISE library block"),
 *   category = @Translation("ARISE Media")
 * )
 */
class LibraryBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    /* get saved configuration */
    $config = $this->getConfiguration();

    $arise_api_key =  $config['arise_api_key'];
    $feedtag = $config['feedtag'];
    $groupid = $config['groupid'];
    $num_items = $config['num_items'];
    $more_link = $config['link_url'];
    $more_text = $config['link_text'];

    $url = 'http://www.preventionweb.net/v2/api/v1/tagged_content/get/output.json?tag='.$feedtag.'&content_type='.$groupid.'&number='.$num_items.'&api_key='.$arise_api_key;

    /* CURL */
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_TIMEOUT, 5);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $data = curl_exec($ch);
    curl_close($ch);

    $json = json_decode($data, true);

    /* init content */
    $content = '';

    return array(
      '#theme' => 'arise_library_feed_block',
      '#content' => $content,
      '#status' => $json['status'],
      '#total' => $json['total'],
      '#num_items' => $num_items,
      '#results' => $json['results'],
      '#more_link' => $more_link,
      '#more_text' => $more_text,
      '#attached' => array(
        'library' =>  array(
          'myblock/base'
        ),
      ),
      '#attributes' => [],
    );

  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $groupid_options = range(1, 99);
    $num_items_options = range(0, 4);

    /* define configuration form items */
    $form['arise_api_key'] = array(
      '#type' => 'textfield',
      '#title' => t('ARISE API key'),
      '#default_value' => $config['arise_api_key'],
      '#maxlength' => 128,
      '#description' => t('ARISE API key for getting feed'),
      '#required' => TRUE,
    );
    $form['feedtag'] = array(
      '#type' => 'textfield',
      '#title' => t('Tag'),
      '#default_value' => $config['feedtag'],
      '#maxlength' => 128,
      '#description' => t('Feed tag.'),
      '#required' => TRUE,
    );
    $form['groupid'] = array(
      '#type' => 'select',
      '#title' => t('Content type'),
      '#options' => array_combine($groupid_options, $groupid_options),
      '#default_value' => $config['groupid'],
      '#description' => t('Content type ID'),
      '#required' => TRUE,
    );
    $form['num_items'] = array(
      '#type' => 'select',
      '#title' => t('Number of items to display'),
      '#options' => array_combine($num_items_options, $num_items_options),
      '#default_value' => $config['num_items'],
      '#description' => $this->t('This will set the maximum allowable number of feed elements in the feed block config form'),
      '#required' => TRUE,
    );
    $form['link_url'] = array(
      '#type' => 'textfield',
      '#title' => t('Link'),
      '#default_value' => $config['link_url'],
      '#maxlength' => 256,
      '#description' => t('More URL'),
      '#required' => TRUE,
    );
    $form['link_text'] = array(
      '#type' => 'textfield',
      '#title' => t('Text'),
      '#default_value' => $config['link_text'],
      '#maxlength' => 256,
      '#description' => t('More text'),
      '#required' => TRUE,
    );
    return $form;
  }
    /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    /* sumbit configuration */
    $this->setConfigurationValue('arise_api_key', $form_state->getValue('arise_api_key'));
    $this->setConfigurationValue('feedtag', $form_state->getValue('feedtag'));
    $this->setConfigurationValue('groupid', $form_state->getValue('groupid'));
    $this->setConfigurationValue('num_items', $form_state->getValue('num_items'));
    $this->setConfigurationValue('link_url', $form_state->getValue('link_url'));
    $this->setConfigurationValue('link_text', $form_state->getValue('link_text'));
  } 
  
}
