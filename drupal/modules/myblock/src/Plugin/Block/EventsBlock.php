<?php
/**
 * @file
 * Contains \Drupal\myblock\Plugin\Block\EventsBlock.
 */
namespace Drupal\myblock\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides ARISE Events from PreventionWeb tagged feed.
 *
 * @Block(
 *   id = "arise_events_block",
 *   admin_label = @Translation("ARISE events block"),
 *   category = @Translation("ARISE Media")
 * )
 */
class EventsBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    /* get saved configuration */
    $config = $this->getConfiguration();

    $arise_api_key =  $config['arise_api_key'];
    $feedtag = $config['feedtag'];
    $groupid = $config['groupid'];
    $num_items = $config['num_items'];
    $more_link = $config['link_url'];
    $more_text = $config['link_text'];

    $url = 'http://www.preventionweb.net/v2/api/v1/tagged_content/get/output.json?tag='.$feedtag.'&content_type='.$groupid.'&number='.$num_items.'&api_key='.$arise_api_key;

    /* CURL */
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_TIMEOUT, 5);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $data = curl_exec($ch);
    curl_close($ch);

    $json = json_decode($data, true);

    /* init content */
    $content = '';

    $status_class = 'status-'.$json['status'];
    
    $results = $json['results'];

    if ($json['status'] == 200) {
      if ($json['total'] > 0) {
        foreach($results as $index => $event) {
          if ($index == $num_items) break;

          $content .= '<div id="event-id-'.$event['id'].'" class="col-xs-12 col-md-3 event">';
          $content .= '<div class="event-location">';

          $locations = $event['locations'];
          $date_start = $locations[0]['date_start'];
          $date_end = $locations[0]['date_end'];

          /*  */
          $time_start = strtotime($date_start);
          $time_end = strtotime($date_end);

          /* split start date */
          $date_start_year = date('Y', $time_start);
          $date_start_month = date('M', $time_start);
          $date_start_day = date('d', $time_start);

          /* split end date */
          $date_end_year = date('Y', $time_end);
          $date_end_month = date('M', $time_end);
          $date_end_day = date('d', $time_end);

          $datecontent = '';
          /* one day event */
          if ($time_start == $time_end) {
            $datecontent .= '<span class="event-date-elem">'.$date_start_day.'&nbsp;'.$date_start_month.'&nbsp;'.$date_start_year.'</span>';
            $date_event_format = $date_start_day.'-'.$date_start_month.'-'.$date_start_year;
          }
          /* on the same year */
          elseif ($date_start_year == $date_end_year) {
            /* on the same month, same year */
            if ($date_start_month == $date_end_month) {
              $datecontent .= '<span class="event-date-elem event-date-day first">'.$date_start_day.'</span>';
              $datecontent .= '<span class="event-date-elem event-date-dash">-</span>';
              $datecontent .= '<span class="event-date-elem event-date-day">'.$date_end_day.'</span>';
              $datecontent .= '<span class="event-date-elem event-date-month">'.$date_start_month.'</span>';
              $datecontent .= '<span class="event-date-elem event-date-year">'.$date_start_year.'</span>';
              $date_event_format = $date_start_day.'/'.$date_end_day.'-'.$date_end_month.'-'.$date_end_year;
            }
            /* on 2 months, same year */
            else {
              $datecontent .= '<span class="event-date-elem event-date-day first">'.$date_start_day.'</span>';
              $datecontent .= '<span class="event-date-elem event-date-month">'.$date_start_month.'</span>';
              $datecontent .= '<span class="event-date-elem event-date-dash">-</span>';
              $datecontent .= '<span class="event-date-elem event-date-day">'.$date_end_day.'</span>';
              $datecontent .= '<span class="event-date-elem event-date-month">'.$date_end_month.'</span>';
              $datecontent .= '<span class="event-date-elem event-date-year last">'.$date_start_year.'</span>';
              $date_event_format = $date_start_day.'-'.$date_start_month.'/'.$date_end_day.'-'.$date_end_month.'-'.$date_end_year;
            }
          }
          /* on 2 years */
          else {
            $datecontent .= '<span class="event-date-elem event-date-day first">'.$date_start_day.'</span>';
            $datecontent .= '<span class="event-date-elem event-date-month">'.$date_start_month.'</span>';
            $datecontent .= '<span class="event-date-elem event-date-year">'.$date_start_year.'</span>';
            $datecontent .= '<span class="event-date-elem event-date-dash">-</span>';
            $datecontent .= '<span class="event-date-elem event-date-day">'.$date_end_day.'</span>';
            $datecontent .= '<span class="event-date-elem event-date-month">'.$date_end_month.'</span>';
            $datecontent .= '<span class="event-date-elem event-date-year last">'.$date_end_year.'</span>';
            $date_event_format = $date_start_day.'-'.$date_start_month.'-'.$date_start_year.'/'.$date_end_day.'-'.$date_end_month.'-'.$date_end_year;
          }

          $event['date_event_format'] = $date_event_format;
          $results[$index] = $event;

          $content .= '<span class="event-location-country">'.$locations[0]['country'].'</span>';
          $content .= '<span class="event-location-city">('.$locations[0]['city'].')</span>';
          $content .= '</div>';
          $content .= '<div class="event-date">'.$datecontent.'</div>';
          $content .= '<div class="event-summary"><a href="'.$event['url'].'" class="event-link">'.$event['title'].'</a></div>';
          $content .= '</div>';
        }

        $content .= '<div class="tab-footer"><span><a class="more-link" href="'.$more_link .'" target="_blank">'.$more_text .'</a></span></div>';
      }
      else {
        $content = '<div class="feed-container container-fluid"><div class=" row error no-feed">No available content.</div></div>';
      }
    }
    else {
      $content = '<div class="feed-container container-fluid"><div class=" row error '.$status_class.'">Remote server error.</div></div>';
    }

    return array(
      '#theme' => 'arise_events_feed_block',
      '#content' => $content,
      '#status' => $json['status'],
      '#total' => $json['total'],
      '#num_items' => $num_items,
      '#results' => $results,
      '#more_link' => $more_link,
      '#more_text' => $more_text,
      '#attached' => array(
        'library' =>  array(
          'myblock/base'
        ),
      ),
      '#attributes' => [],
    );

  }
    /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $groupid_options = range(1, 99);
    $num_items_options = range(0, 4);

    /* define configuration form items */
    $form['arise_api_key'] = array(
      '#type' => 'textfield',
      '#title' => t('ARISE API key'),
      '#default_value' => $config['arise_api_key'],
      '#maxlength' => 128,
      '#description' => t('ARISE API key for getting feed.'),
      '#required' => TRUE,
    );
    $form['feedtag'] = array(
      '#type' => 'textfield',
      '#title' => t('Tag'),
      '#default_value' => $config['feedtag'],
      '#maxlength' => 128,
      '#description' => t('Feed tag.'),
      '#required' => TRUE,
    );
    $form['groupid'] = array(
      '#type' => 'select',
      '#title' => t('Content type'),
      '#options' => array_combine($groupid_options, $groupid_options),
      '#default_value' => $config['groupid'],
      '#description' => t('Content type ID.'),
      '#required' => TRUE,
    );
    $form['num_items'] = array(
      '#type' => 'select',
      '#title' => t('Number of items to display'),
      '#options' => array_combine($num_items_options, $num_items_options),
      '#default_value' => $config['num_items'],
      '#description' => $this->t('This will set the maximum allowable number of feed elements in the feed block config form'),
      '#required' => TRUE,
    );
    $form['link_url'] = array(
      '#type' => 'textfield',
      '#title' => t('Link'),
      '#default_value' => $config['link_url'],
      '#maxlength' => 256,
      '#description' => t('Feed tag.'),
      '#required' => TRUE,
    );
    $form['link_text'] = array(
      '#type' => 'textfield',
      '#title' => t('Text'),
      '#default_value' => $config['link_text'],
      '#maxlength' => 256,
      '#description' => t('Feed tag.'),
      '#required' => TRUE,
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->setConfigurationValue('arise_api_key', $form_state->getValue('arise_api_key'));
    $this->setConfigurationValue('link_text', $form_state->getValue('link_text'));
    $this->setConfigurationValue('groupid', $form_state->getValue('groupid'));
    $this->setConfigurationValue('num_items', $form_state->getValue('num_items'));
    $this->setConfigurationValue('feedtag', $form_state->getValue('feedtag'));
    $this->setConfigurationValue('link_url', $form_state->getValue('link_url'));
  }

}
