<?php

/**
 * @file
 * Contains \Drupal\system\Form\ImageToolkitForm.
 */

namespace Drupal\arise_json_feed\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;

/**
 * Configures image toolkit settings for this site.
 */
class AriseJsonFeedSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'arise_json_feed.settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['arise_json_feed.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('arise_json_feed.settings');
    $max_feed_array = range(1, 4);
    $content_type_array = range(1, 99);

    $form['arise_api_key'] = array(
      '#type' => 'textfield',
      '#title' => t('ARISE API key'),
      '#default_value' => $config->get('arise_api_key'),
      '#maxlength' => 128,
      '#description' => t('ARISE API key for getting feed.'),
      '#required' => TRUE,
    );

    $form['feedtag'] = array(
      '#type' => 'textfield',
      '#title' => t('Tag'),
      '#default_value' => $config->get('feedtag'),
      '#maxlength' => 128,
      '#description' => t('Feed tag.'),
      '#required' => TRUE,
    );

    $form['content_type'] = array(
      '#type' => 'select',
      '#title' => t('Content type'),
      '#options' => array_combine($content_type_array, $content_type_array),
      '#default_value' => $config->get('content_type'),
      '#description' => t('Content type ID.'),
      '#required' => TRUE,
    );

    $form['max_feed'] = array(
      '#type' => 'select',
      '#title' => t('Maximum number of items to display'),
      '#options' => array_combine($max_feed_array, $max_feed_array),
      '#default_value' => $config->get('max_feed'),
      '#description' => $this->t('This will set the maximum allowable number of feed elements in the feed block config form'),
      '#required' => TRUE,
    );

    return parent::buildForm($form, $form_state);
  }


  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('arise_json_feed.settings');

    $config->set('feedtag', $form_state->getValue('feedtag'))
      ->set('arise_api_key', $form_state->getValue('arise_api_key'))
      ->set('content_type', $form_state->getValue('content_type'))
      ->set('max_feed', $form_state->getValue('max_feed'));
    $config->save();

    parent::submitForm($form, $form_state);
  }

}
