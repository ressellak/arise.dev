<?php
/**
 * @file
 * Contains \Drupal\myblock\Plugin\Block\MembersBlock.
 */
namespace Drupal\myblock\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides ARISE members list.
 *
 * @Block(
 *   id = "arise_members_block",
 *   admin_label = @Translation("ARISE members block"),
 *   category = @Translation("ARISE Media")
 * )
 */
class MembersBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    /* get saved configuration */
    $config = $this->getConfiguration();

    $arise_api_key =  $config['arise_api_key'];
    $groupid = $config['groupid'];
    $num_items = $config['num_items'];
    $more_link = $config['link_url'];
    $more_text = $config['link_text'];
    $with_sort_filter = $config['with_sort_filter'];

    $url = 'http://www.preventionweb.net/v2/api/v1/organizations/get/output.json?group_id='.$groupid.'&number='.$num_items.'&api_key='.$arise_api_key;

    /* CURL */
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_TIMEOUT, 5);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $data = curl_exec($ch);
    curl_close($ch);

    $json = json_decode($data, true);
    /* data control */
    $data_error = (count($json['results']) != $json['total']);

    /* this array will contain restrained country ids mapped to country names: id (int) => country_name (string)
       we will use this list to add a country name sorting attribute in members list. This attribute is equivalent to its position in this array
    */
    $country_unique_list_sorted = [];
    $member_list_sorted = [];

    /* build country list */
    foreach($json['results'] as $result) {
      if (!in_array($result['ctry_title'], $country_unique_list_sorted)) {
        $country_unique_list_sorted[$result['org_ctry_id']] = $result['ctry_title'];
      }
      $member_list_sorted[$result['org_id']] = $result['org_title_long'];
    }

    /* sort: order by country names */
    asort($country_unique_list_sorted, SORT_FLAG_CASE);
    /* sort: order by member names */
    asort($member_list_sorted);

    /* in this loop we  */
    foreach($json['results'] as $index => $result) {
      $result['data_ctry_title_init_order'] = $index + 1;

      $org_ctry_id_pos = array_search($result['org_ctry_id'], array_keys($country_unique_list_sorted)) + 1;
      $result['data_ctry_title_order'] = $org_ctry_id_pos;

      $member_id_pos = array_search($result['org_id'], array_keys($member_list_sorted)) + 1;
      $result['data_org_title_long_order'] = $member_id_pos;

      $json['results'][$index] = $result;
    }

    $content = '';

    return array(
      '#theme' => 'arise_members_feed_block',
      '#content' => $content,
      '#status' => $json['status'],
      '#total' => $json['total'],
      '#num_items' => $num_items,
      '#results' => $json['results'],
      '#more_link' => $more_link,
      '#more_text' => $more_text,
      '#with_sort_filter' => $with_sort_filter,
      '#country_unique_list_sorted' => $country_unique_list_sorted,
      '#data_error' => $data_error,
      '#attached' => array(
        'library' =>  array(
          'myblock/base',
          'myblock/sort-filter'
        ),
      ),
      '#attributes' => [],
    );

  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    /* get saved configuration */
    $config = $this->getConfiguration();

    $groupid_options = range(1, 99);
    $num_items_options = range(0, 4);

    $form['arise_api_key'] = array(
      '#type' => 'textfield',
      '#title' => t('ARISE API key'),
      '#default_value' => $config['arise_api_key'],
      '#maxlength' => 128,
      '#description' => t('ARISE API key for getting feed'),
      '#required' => TRUE,
    );
    $form['groupid'] = array(
      '#type' => 'select',
      '#title' => t('Content type'),
      '#options' => array_combine($groupid_options, $groupid_options),
      '#default_value' => $config['groupid'],
      '#description' => t('Content type ID'),
      '#required' => TRUE,
    );
    $form['num_items'] = array(
      '#type' => 'select',
      '#title' => t('Number of items to display'),
      '#options' => array_combine($num_items_options, $num_items_options),
      '#default_value' => $config['num_items'],
      '#description' => $this->t('This will set the maximum allowable number of feed elements in the feed block config form'),
      '#required' => TRUE,
    );
    $form['link_url'] = array(
      '#type' => 'textfield',
      '#title' => t('Link'),
      '#default_value' => $config['link_url'],
      '#maxlength' => 256,
      '#description' => t('More URL'),
      '#required' => FALSE,
    );
    $form['link_text'] = array(
      '#type' => 'textfield',
      '#title' => t('Text'),
      '#default_value' => $config['link_text'],
      '#maxlength' => 256,
      '#description' => t('More text'),
      '#required' => FALSE,
    );
    $form['with_sort_filter'] = array(
      '#type' => 'select',
      '#title' => t('Add filter and sorting form'),
      '#default_value' => $config['with_sort_filter'],
      '#options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
      ),
      '#description' => t('Select if you want to add filter and sorting form'),
      '#required' => TRUE,
    );
    return $form;
  }
    /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    /* sumbit configuration */
    $this->setConfigurationValue('arise_api_key', $form_state->getValue('arise_api_key'));
    $this->setConfigurationValue('groupid', $form_state->getValue('groupid'));
    $this->setConfigurationValue('num_items', $form_state->getValue('num_items'));
    $this->setConfigurationValue('link_url', $form_state->getValue('link_url'));
    $this->setConfigurationValue('link_text', $form_state->getValue('link_text'));
    $this->setConfigurationValue('with_sort_filter', $form_state->getValue('with_sort_filter'));
  } 

}
