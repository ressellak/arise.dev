<?php
/**
 * @file
 * Contains \Drupal\banner_block\Plugin\Block\BannerBlock.
 */
namespace Drupal\banner_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides banner block including image, quote text, link and description.
 *
 * @Block(
 *   id = "arise_banner_block",
 *   admin_label = @Translation("ARISE banner block"),
 *   category = @Translation("ARISE Media")
 * )
 */
class BannerBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    /* get saved configuration */
    $config = $this->getConfiguration();

    $banner_relative_path = $config['banner_relative_path'];
    $quote = $config['quote'];
    $credit = $config['credit'];
    $more_link = $config['more_link'];
    $more_text = $config['more_text'];
    $description_text =  $config['description_text'];

    /* init content */
    $content = '';

    $theme = \Drupal::theme()->getActiveTheme()->getName();
    
    if (isset($_SERVER['DOCUMENT_ROOT']) && file_exists($_SERVER['DOCUMENT_ROOT'].'/'.$banner_relative_path)) {
      $banner = base_path().$banner_relative_path;
    }
    else {
      \Drupal::logger('banner_block')->notice('Relative path to banner image is not valid or file does not exist. Banner wil be set to default.');
      $banner = base_path().drupal_get_path('theme', $theme).'/images/default-homepage-banner.jpg';
    }

    return array(
      '#theme' => 'arise_banner_block',
      '#content' => $content,
      '#banner' => $banner,
      '#quote' => $quote,
      '#credit' => $credit,
      '#description_text' => $description_text,
      '#more_link' => $more_link,
      '#more_text' => $more_text,
      '#attached' => array(
        'library' =>  array(
          'banner_block/base'
        ),
      ),
      '#attributes' => [],
    );

  }
    /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    /* define configuration form items */
    $form['banner_relative_path'] = array(
      '#type' => 'textfield',
      '#title' => t('Banner image relative path'),
      '#default_value' => $config['banner_relative_path'],
      '#maxlength' => 128,
      '#description' => t('Please, copy first image file to theme directory and specify here relative to it. If path is invalid or file does not exist, default image will be userd.'),
      '#required' => TRUE,
    );
    $form['quote'] = array(
      '#type' => 'textarea',
      '#rows' => 1,
      '#resizable' => 'both',
      '#title' => t('Quote text'),
      '#default_value' => $config['quote'],
      '#description' => t('Copy quote text.'),
      '#required' => TRUE,
    );
    $form['credit'] = array(
      '#type' => 'textfield',
      '#title' => t('Quote credit'),
      '#default_value' => $config['credit'],
      '#maxlength' => 128,
      '#description' => t('Copy quote credit text.'),
      '#required' => TRUE,
    );
    $form['more_link'] = array(
      '#type' => 'textfield',
      '#title' => t('More link URL'),
      '#default_value' => $config['more_link'],
      '#maxlength' => 256,
      '#description' => t('Add "More link" URL'),
      '#required' => TRUE,
    );
    $form['more_text'] = array(
      '#type' => 'textfield',
      '#title' => t('More link URL'),
      '#default_value' => $config['more_text'],
      '#maxlength' => 256,
      '#description' => t('Add "More link" text'),
      '#required' => TRUE,
    );
    $form['description_text'] = array(
      '#type' => 'textarea',
      '#rows' => 1,
      '#resizable' => 'both',
      '#title' => t('Description text'),
      '#default_value' => $config['description_text'],
      '#description' => t('Copy here long text describing ARISE.'),
      '#required' => TRUE,
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->setConfigurationValue('banner_relative_path', $form_state->getValue('banner_relative_path'));
    $this->setConfigurationValue('quote', $form_state->getValue('quote'));
    $this->setConfigurationValue('credit', $form_state->getValue('credit'));
    $this->setConfigurationValue('more_link', $form_state->getValue('more_link'));
    $this->setConfigurationValue('more_text', $form_state->getValue('more_text'));
    $this->setConfigurationValue('description_text', $form_state->getValue('description_text'));
  }

}
