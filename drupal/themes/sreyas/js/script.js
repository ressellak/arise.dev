(function($) {
  // $.fn.mathSpace = function() {
    // return $(this).each(function(){
      // $(this).children('span').each(function() {
        // var el = $(this);
        // var text = el.text();
        // el.text(text.split(' ').join('\u205f')); 
      // });
    // });
  // }

  $(document).ready(function() {
    // $('.debug-col').html('window width: ' + $( window ).width() + ' | document width: ' + $( document ).width());

    /* top corporate menu */
    $('.corporatetop-menu-igniter').click(function() {
      $('.corporatetop-dropdown-menu').slideToggle();
    });
    $('.corporatetop-menu-closer').click(function() {
      $('.corporatetop-dropdown-menu').slideToggle();
    });

    /* to top arrow */
    $().UItoTop({ easingType: 'easeOutQuart' });
    
    // // $('.juhana-jquery .quote-long-text').wraplines({
      // // lineClass: 'paddedline'
    // // });
    
    // // $('.dave-rupert-unicode').mathSpace();

    if ($('.publication-abstract').length) {
      $('.publication-abstract').readmore({
        moreLink: '<a href="#">Read more abstract</a>',
        lessLink: '<a href="#">Close</a>',
        speed: 200,
        collapsedHeight: 160,
        embedCSS: false,
        afterToggle: function(trigger, element, expanded) {
          if (!expanded) {
            $('html, body').animate({scrollTop: element.parents('.publication').offset().top}, {duration: 200});
          }
        },
      });
    }

    if ($('.total-members-hidden').length) {
      if ($('.quote-text-top').length) {
        var total_members = $('.total-members-hidden').text();
        var inner_text = $('.quote-text-top').html();
        $('.quote-text-top').html('<span class="total-members">'+ total_members + '</span>&nbsp;' + inner_text);
      }
    }

  });
})(jQuery);
